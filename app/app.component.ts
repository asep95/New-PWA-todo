import { Component } from '@angular/core';
import {sampletodo} from './sample-todo';
import {sampletodoservice} from './sample-todo.service';
import { NgModule } from '@angular/core';
import { NgForm} from '@angular/forms';
import { FormsModule} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [sampletodoservice]
})
export class AppComponent {

  newTodo: sampletodo = new sampletodo();

  constructor(private todoDataService: sampletodoservice) {
  }
  addTodo() {
    this.todoDataService.addTodo(this.newTodo);
    this.newTodo = new sampletodo();
  }

  toggleTodoComplete(todo) {
    this.todoDataService.toggleTodoComplete(todo);
  }

  removeTodo(todo) {
    this.todoDataService.deleteTodoById(todo.id);
  }

  get todos() {
    return this.todoDataService.getAllTodos();
  }
}
